#include <unistd.h>
#include <sys/types.h>
#include <limits.h>
#include <stdio.h>
#include <string.h>

int main(int argc, char **argv) {
    setreuid(geteuid(), geteuid());
    setregid(getegid(), getegid());

    char linkPath[PATH_MAX];
    pid_t pid = getpid();
    sprintf(linkPath, "/proc/%d/exe", pid);

    char exePath[PATH_MAX];
    memset(exePath, 0, sizeof(exePath));

    if (readlink(linkPath, exePath, PATH_MAX) == -1)
    {
        printf("Failed to get executable path");
        return 1;
    }

    char scriptPath[PATH_MAX];
    snprintf(scriptPath, PATH_MAX, "%s.py", exePath);

    pid_t childPid = fork();
    if (childPid >= 0) {
        if (childPid == 0) {
            // We are in the child process, start the external Python script:
            return execv(scriptPath, argv);
        } else {
            // We are in the parent process, wait for the child and return:
            int status;
            waitpid(childPid, &status, 0);
            return status;
        }
    } else {
        return 1; // Fork failed
    }
}
