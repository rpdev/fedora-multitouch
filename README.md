This repository provides RPM spec files which can be used to create
RPM packages for [Fedora](https://getfedora.org/) and related distributions.

# Usage

If you are rather an end user, there's no need for you to deal
with the SPECs. Instead, there's a repository prepared on
[Copr](https://copr.fedoraproject.org/). To get more information
(especially about the current build status of packages), visit the
[fedora-multitouch](https://copr.fedoraproject.org/coprs/mhoeher/multitouch/)
Copr repository.

In order to enable the repository and install packages, just run

	dnf copr enable mhoeher/multitouch

as admin. Afterwards, you should be able to install individual packages from it.
In particular, you might want to install either the *libinput-gestures*
package or the combination *touchegg* and *touchegg-gce*. Note that due to
Fedora is switching to libinput-based drivers, you should try the
*libinput-gestures* solution first and only if this does not work, try to fall
back to *touchegg*.

# Building RPMs

If you want to build the RPM packages on your own, you probably want to
first read through the
[How to create an RPM package](https://fedoraproject.org/wiki/How_to_create_an_RPM_package)
wiki page, which describes the procedure in a quite concise manner.
After creating your RPM build environment, copy the files contained
in this repositories SPECS directory into your build roots SPECS one.
For each package you want to build, download the appropriate source
package into the SOURCES directory of your RPM build root.
Then change into your build root and issue

	rpmbuild -ba ./SPECS/$SOME_SPEC_NAME.spec

to build the appropriate RPM for each package you are interested in.

# Reporting Issues

If you encounter any issues (be it when using the RPMs in the
Copr repository or when trying to build RPMs using the provided SPECs,
please report this to this projects
[issue tracker](https://gitlab.com/rpdev/fedora-multitouch/issues).

