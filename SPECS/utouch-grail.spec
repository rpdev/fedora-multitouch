Name:           utouch-grail
Version:        3.1.1
Release:        0%{?dist}
Summary:        Gesture Recognition And Instantiation Library

License:        LGPLv3
URL:            https://launchpad.net/grail
Source0:        https://launchpad.net/grail/trunk/3.1.1/+download/grail-3.1.1.tar.bz2

BuildRequires:  utouch-evemu-devel >= 1.0.10
BuildRequires:  utouch-frame-devel >= 2.5.0
BuildRequires:  libX11-devel
BuildRequires:  libXext-devel
BuildRequires:  libXi-devel
BuildRequires:  xorg-x11-server-devel
BuildRequires:  asciidoc
Buildrequires:  xmlto
Requires:       utouch-evemu >= 1.0.10
Requires:       utouch-frame >= 2.5.0

%description
This tree consists of an interface and tools for handling gesture
recognition and gesture instantiation.

When a multitouch gesture is performed on a device, the recognizer
emits one or several possible gestures. Once the context of the
gesture is known, i.e., in what window the touches land and what
gestures the clients of that window listens to, the instantiator
delivers the matching set of gestures.

The library handles tentative getures, i.e., buffering of events for
several alternative gestures until a match is confirmed.


%prep
%autosetup -n grail-%{version}


%build
%configure
# Disable rpath:
sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool
make %{?_smp_mflags} CFLAGS+='-Wformat -Wno-misleading-indentation'

%install
rm -rf $RPM_BUILD_ROOT
%make_install


%define _unpackaged_files_terminate_build 0 

%files
%doc README COPYING
%{_libdir}/*.so.*

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig



%package devel
Summary: Development files for evemu
Requires: utouch-grail >= %{version}

%description devel
This package provides the files required for developing
applications and libraries using utouch-evemu.

%files devel
%{_includedir}/*
%{_libdir}/*.a
%{_libdir}/*.la
%{_libdir}/*.so
%{_libdir}/pkgconfig/*
 


%changelog
* Sat Nov 26 2016 Updated to 3.1.1-0
* Wed Jul 29 2015 Initial spec file for version 3.1.0-1
- 
