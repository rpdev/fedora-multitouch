Name:           utouch-evemu
Version:        1.0.10 
Release:        3%{?dist}
Summary:        Provides a programmatic API to access the kernel input event devices

License:        LGPLv3
URL:            http://bitmath.org/code/evemu/
Source0:        https://launchpad.net/evemu/trunk/evemu-1.0.10/+download/evemu-1.0.10.tar.gz

BuildRequires:  asciidoc
Buildrequires:  xmlto
#Requires:       

%description
The evemu library and tools are used to describe devices, record data,
create devices and replay data from kernel evdev devices. 

%prep
%autosetup -n evemu-%{version}


%build
%configure
# Disable rpath:
sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool
make %{?_smp_mflags} CFLAGS+='-Wformat'

%install
rm -rf $RPM_BUILD_ROOT
%make_install


%define _unpackaged_files_terminate_build 0 

%files
%doc README COPYING
%{_bindir}/*
%{_libdir}/*.so.*
%{_datadir}/man/man1/*

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig



%package devel
Summary: Development files for evemu
Requires: utouch-evemu >= %{version}

%description devel
This package provides the files required for developing
applications and libraries using utouch-evemu.

%files devel
%{_includedir}/*
%{_libdir}/*.a
%{_libdir}/*.la
%{_libdir}/*.so
%{_libdir}/pkgconfig/*
 


%changelog
* Sun Jul 26 2015 Initial spec file for version 1.0.10-1
- 
