Name:           touchegg
Version:        1.1.1
Release:        0%{?dist}
Epoch:          1
Summary:        A multitouch gesture recognizer for GNU/Linux

License:        LGPLv2
URL:            https://github.com/JoseExposito/touchegg
Source0:        https://github.com/JoseExposito/touchegg/archive/1.1.1.tar.gz

BuildRequires:  utouch-geis-devel >= 2.2.17
BuildRequires:  libX11-devel
BuildRequires:  libXext-devel
BuildRequires:  libXtst-devel
BuildRequires:  qt-devel
Requires:       utouch-geis >= 2.2.17
Requires:       qt

%description
Touchégg is a multitouch gesture recognizer for 
GNU/Linux that allows associating actions to each gesture. 

%prep
%setup
# %patch


%build
cd touchegg
mkdir touchegg-build
cd touchegg-build
qmake-qt4 CONFIG+=release ../touchegg.pro
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
cd touchegg/touchegg-build
make install INSTALL_ROOT="%{buildroot}"


%files
%{_bindir}/*
%{_datadir}/touchegg/*



%changelog
* Sat Nov 26 2016 Update to release 1.1.1
* Fri Jul 31 2015 Initial spec file for Git rev. from 2015.07.31
- 
