Name:           utouch-frame
Version:        2.5.0
Release:        3%{?dist}
Summary:        Touch Frame Library

License:        LGPLv3
URL:            https://launchpad.net/frame
Source0:        https://launchpad.net/frame/trunk/v2.5.0/+download/frame-2.5.0.tar.gz

BuildRequires:  utouch-evemu-devel >= 1.0.10
BuildRequires:  libX11-devel
BuildRequires:  libXext-devel
BuildRequires:  libXi-devel
BuildRequires:  xorg-x11-server-devel
BuildRequires:  asciidoc
Buildrequires:  xmlto


%description
This tree handles the buildup and synchronization of a set of
simultaneous touches. The library is input agnostic.

%prep
%autosetup -n frame-%{version}


%build
%configure
# Disable rpath:
sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool
make %{?_smp_mflags} CFLAGS+='-Wformat'

%install
rm -rf $RPM_BUILD_ROOT
%make_install


%define _unpackaged_files_terminate_build 0 

%files
%doc README COPYING
%{_libdir}/*.so.*

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig



%package devel
Summary: Development files for evemu
Requires: utouch-frame >= %{version}

%description devel
This package provides the files required for developing
applications and libraries using utouch-evemu.

%files devel
%{_includedir}/*
%{_libdir}/*.a
%{_libdir}/*.la
%{_libdir}/*.so
%{_libdir}/pkgconfig/*
 


%changelog
* Wed Jul 29 2015 Initial spec file for version 2.5.0-1
- 
