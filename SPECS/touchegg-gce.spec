Name:           touchegg-gce
Version:        0.3.1
Release:        0%{?dist}
Epoch:          1
Summary:        GUI to configure touchegg

License:        GPLv3
URL:            https://github.com/Raffarti/Touchegg-gce
Source0:        https://github.com/Raffarti/Touchegg-gce/archive/1.3.1.tar.gz

BuildRequires:  utouch-geis-devel >= 2.2.17
BuildRequires:  libX11-devel
BuildRequires:  qt-devel
Requires:       qt
Requires:       touchegg

%description
Touchegg-gce is a graphical user interface for touchegg.

%prep
%setup
#%patch

%build
mkdir build
cd build
qmake-qt4 CONFIG+=release PREFIX="%{buildroot}/usr" CONFIG_PATH="%{buildroot}/etc" ..
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
cd build
make install


%files
%doc Changelog COPYING README
%{_bindir}/*
%{_datadir}/touchegg-gce/*
%{_datadir}/icons/*
%{_datadir}/applications/*
%{_sysconfdir}/touchegg-gce.conf


%changelog
* Sat Nov 26 2016 Updated to release 0.3.1-0
* Tue Aug 18 2015 Updated after bug fixes in touchegg-gce

* Sat Aug 01 2015 Initial spec file for Git rev. from 2015.08.01
- 
