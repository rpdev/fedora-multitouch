Name:           touchegg-gui
Version:        2015.07.31
Release:        1%{?dist}
Summary:        GUI for touchegg

License:        LGPLv2
URL:            https://github.com/JoseExposito/touchegg
Source0:        https://github.com/JoseExposito/touchegg/archive/master.zip

BuildRequires:  qt-devel
Requires:       qt

%description
A graphical user interface for configuring touchegg.

%prep
%autosetup -n touchegg-master


%build
mkdir touchegg-gui-build
cd touchegg-gui-build
qmake-qt4 CONFIG+=release ../touchegg-gui
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
cd touchegg-gui-build
make install INSTALL_ROOT="%{buildroot}"


%files
%doc touchegg-gui/COPYING touchegg/COPYRIGHT touchegg/README
%{_bindir}/*
%{_datadir}/applications/*
%{_datadir}/icons/*



%changelog
* Fri Jul 31 2015 Initial spec file for Git rev. from 2015.07.31
- 
