Name:           utouch-geis
Version:        2.2.17
Release:        4%{?dist}
Summary:        Gesture Engine Implementation Support

License:        LGPLv3
URL:            https://launchpad.net/geis
Source0:        https://launchpad.net/geis/trunk/2.2.17/+download/geis-2.2.17.tar.xz

BuildRequires:  utouch-evemu-devel >= 1.0.10
BuildRequires:  utouch-frame-devel >= 2.5.0
BuildRequires:  utouch-grail-devel >= 3.1.0
BuildRequires:  dbus-devel
BuildRequires:  libxcb-devel
BuildRequires:  libX11-devel
BuildRequires:  libXext-devel
BuildRequires:  libXi-devel
BuildRequires:  python3-devel
BuildRequires:  xorg-x11-server-devel
BuildRequires:  asciidoc
Buildrequires:  xmlto
Requires:       utouch-evemu >= 1.0.10
Requires:       utouch-frame >= 2.5.0
Requires:       utouch-grail >= 3.1.0
Requires:       dbus
Requires:       python3

%description
GEIS (Gesture Engine Implementation Support) library
built on top of the XCB bindings to the GRAIL gesture recognition and
instantiation library.


%prep
%autosetup -n geis-%{version}


%build
%configure
# Disable rpath:
sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool
make %{?_smp_mflags} CFLAGS+='-Wformat -Wno-misleading-indentation'

%install
rm -rf $RPM_BUILD_ROOT
%make_install

for file in %{buildroot}/%{_usr}/lib/python*/site-packages/geisview/__init__.py; do
    chmod a+x ${file}
done
#%%define _unpackaged_files_terminate_build 0 

%files
%doc README COPYING
%{_bindir}/*
%{_libdir}/*.so.*
%{_libdir}/python*
%{_usr}/lib/python*
%{_datadir}/applications/*
%{_datadir}/doc/geis/*
%{_datadir}/geisview
%{_datadir}/man/man1/*
%{_datadir}/pixmaps/*

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig



%package devel
Summary: Development files for evemu
Requires: utouch-grail >= %{version}

%description devel
This package provides the files required for developing
applications and libraries using utouch-evemu.

%files devel
%{_includedir}/*
%{_libdir}/*.a
%{_libdir}/*.la
%{_libdir}/*.so
%{_libdir}/pkgconfig/*
 


%changelog
* Wed Jul 29 2015 Initial spec file for version 2.2.17-1
- 
