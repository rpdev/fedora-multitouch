Name:           libinput-gestures
Version:        2.73
Release:        1%{?dist}
Summary:        Actions gestures on your touchpad using libinput 

License:        GPL
URL:            https://github.com/bulletmark/libinput-gestures
Source0:        https://github.com/bulletmark/libinput-gestures/archive/%{version}.tar.gz
Source1:        libinput-gestures.c
Patch0:         libinput-gestures-0.patch

BuildRequires:  make
BuildRequires:  gcc
Requires:       libinput
Requires:       xdotool
Requires:       wmctrl

%description
Libinput-gestures is a utility which reads libinput gestures from 
your touchpad and maps them to gestures you configure in a configuration file.
Each gesture can be configured to activate a shell command which is typically an 
xdotool command to action desktop/window/application keyboard combinations and 
commands.

%global debug_package %{nil}

%prep
%setup


%build
# Nothing to do


%install
mkdir -p "%{buildroot}/usr/bin"
mkdir -p "%{buildroot}/usr/share/applications"
mkdir -p "%{buildroot}/usr/share/icons/hicolor/128x128/apps"
mkdir -p "%{buildroot}/usr/share/doc/libinput-gestures"
mkdir -p "%{buildroot}/etc"
make install DESTDIR="%{buildroot}"
mv "%{buildroot}%{_bindir}/libinput-gestures" "%{buildroot}%{_bindir}/libinput-gestures.py"
gcc -o "%{buildroot}%{_bindir}/libinput-gestures" ../../SOURCES/libinput-gestures.c


%files
%attr(2755, root, input) %{_bindir}/libinput-gestures
%{_bindir}/libinput-gestures.py
%{_bindir}/libinput-gestures-setup
%{_datadir}/icons/hicolor/128x128/apps/libinput-gestures.svg
%{_datadir}/applications/libinput-gestures.desktop
%{_datadir}/doc/libinput-gestures/README.md
%{_prefix}/lib/systemd/user/libinput-gestures.service
%{_sysconfdir}/libinput-gestures.conf


%changelog
* Sat May 14 2022 Update to 2.73
* Sat Oct 31 2020 Update to 2.52
* Sat May 09 2020 Update to 2.50
* Sun Jun 02 2019 Update to 2.44
* Mon Nov 05 2018 Update to 2.39
* Sun May 13 2018 Update to 2.35
* Sun Oct 01 2017 Update to 2.29
* Sun Jul 30 2017 Update to 2.27, remove patch (merged to master)
* Fri Jul 07 2017 Update to release 2.26
* Fri Jun 16 2017 Update to release 2.24, less intrusive patching
* Thu Jun 15 2017 Update to release 2.23
* Sat Nov 26 2016 Initial Release 2.13
-
